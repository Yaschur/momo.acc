﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace momo.acc.domain.Timing
{
	public class SolidDays : IDays
	{
		public SolidDays(DateTime dayDate1, DateTime dayDate2)
		{
			_start = dayDate1.Date;
			_finish = dayDate2.Date;
			if (_start > _finish)
			{
				DateTime t = _start;
				_start = _finish;
				_finish = t;
			}
		}

		public DateTime FirstDay { get { return _start; } }

		public DateTime LastDay { get { return _finish; } }

		public bool IsStartedBefore(IDays days)
		{
			throw new NotImplementedException();
		}

		public bool IsFinishedAfter(IDays days)
		{
			throw new NotImplementedException();
		}

		public bool IsIntersectedBy(IDays days)
		{
			throw new NotImplementedException();
		}

		public IEnumerator<DateTime> GetEnumerator()
		{
			return new SolidDaysEnumerator(this);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private readonly DateTime _start;
		private readonly DateTime _finish;
	}
}