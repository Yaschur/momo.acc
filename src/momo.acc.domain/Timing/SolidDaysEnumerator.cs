﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace momo.acc.domain.Timing
{
	class SolidDaysEnumerator : IEnumerator<DateTime>
	{
		public SolidDaysEnumerator(IDays days)
		{
			_start = days.FirstDay;
			_finish = days.LastDay;
			Reset();
		}

		public DateTime Current { get; private set; }

		object IEnumerator.Current { get { return Current; } }

		public void Dispose() { }

		public bool MoveNext()
		{
			if (Current == _finish)
				return false;
			if (_reset)
			{
				_reset = false;
				Current = _start;
				return true;
			}
			Current = Current.AddDays(1);
			return true;
		}

		public void Reset()
		{
			_reset = true;
			Current = default(DateTime);
		}

		private bool _reset;
		private readonly DateTime _start;
		private readonly DateTime _finish;
	}
}
