﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace momo.acc.domain.Timing
{
	public class Day : IDays
	{
		public Day(DateTime dateOfDay)
		{
			_date = dateOfDay.Date;
		}

		public DateTime FirstDay { get { return _date; } }

		public DateTime LastDay { get { return _date; } }

		public bool IsStartedBefore(IDays days)
		{
			return _date < days.LastDay;
		}

		public bool IsFinishedAfter(IDays days)
		{
			return _date > days.FirstDay;
		}

		public bool IsIntersectedBy(IDays days)
		{
			return _date >= days.FirstDay && _date <= days.LastDay;
		}

		public IEnumerator<DateTime> GetEnumerator()
		{
			return new SolidDaysEnumerator(this);
		}


		private readonly DateTime _date;

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}