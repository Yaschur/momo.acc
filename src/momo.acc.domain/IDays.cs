﻿using System;
using System.Collections.Generic;

namespace momo.acc.domain
{
	/// <summary>
	/// A sequence of days, period
	/// </summary>
	public interface IDays : IEnumerable<DateTime>
	{
		/// <summary>
		/// Date of first day in sequence
		/// </summary>
		DateTime FirstDay { get; }

		/// <summary>
		/// Date of last day in sequence
		/// </summary>
		DateTime LastDay { get; }

		/// <summary>
		/// Checks if this period starts before another starts
		/// </summary>
		/// <param name="days">Another period</param>
		/// <returns>True if before another, false otherwise</returns>
		bool IsStartedBefore(IDays days);

		/// <summary>
		/// Checks if this period finishes after another finishes
		/// </summary>
		/// <param name="days">Another period</param>
		/// <returns>True if after, false otherwise</returns>
		bool IsFinishedAfter(IDays days);

		/// <summary>
		/// Checks if this period is intersected by another
		/// </summary>
		/// <param name="days">Another period</param>
		/// <returns>True if intersected, false otherwise</returns>
		bool IsIntersectedBy(IDays days);
	}
}
