﻿using System;

namespace momo.acc.domain.Money
{
	public class MoneyTalk
	{
		public MoneyTalk(DateTime firstOrLastDay, DateTime lastOrFirstDay, int sum)
		{

			FirstDay = firstOrLastDay.Date;
			LastDay = lastOrFirstDay.Date;
			if (FirstDay > LastDay)
			{
				DateTime t = FirstDay;
				FirstDay = LastDay;
				LastDay = FirstDay;
			}
			Sum = sum;
		}

		public readonly DateTime FirstDay;

		public readonly DateTime LastDay;

		public readonly int Sum;
	}
}
