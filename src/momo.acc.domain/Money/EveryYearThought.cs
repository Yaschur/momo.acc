﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace momo.acc.domain.Money
{
	public class EveryYearThought : BaseMoneyThought, IMoneyThought
	{
		public EveryYearThought(int sum)
		{
			_sum = sum;
		}

		public MoneyTalk[] GenerateFlow(DateTime day1, DateTime day2)
		{
			DateTime minDt = day1 > day2 ? day2 : day1;
			DateTime finDate = day2 > day1 ? day2 : day1;
			DateTime startDate = new DateTime(minDt.Year, 1, 1);

			return EnumerateTalks(startDate, finDate)
				.ToArray();
		}

		public MoneyTalk AggregateFlow(DateTime day1, DateTime day2)
		{
			DateTime firstDay = day1 > day2 ? day2 : day1;
			DateTime lastDay = day1 > day2 ? day1 : day2;
			firstDay = new DateTime(firstDay.Year, 1, 1);
			lastDay = new DateTime(lastDay.Year, 12, DateTime.DaysInMonth(lastDay.Year, 12));
			int diff = lastDay.Year - firstDay.Year + 1;

			return new MoneyTalk(firstDay, lastDay, diff * _sum);
		}

		private int _sum;

		private IEnumerable<MoneyTalk> EnumerateTalks(DateTime firstDayDate, DateTime finDate)
		{
			for (DateTime cDate = firstDayDate; cDate <= finDate; cDate = cDate.AddYears(1))
			{
				yield return new MoneyTalk(
					cDate,
					new DateTime(cDate.Year, 12, DateTime.DaysInMonth(cDate.Year, cDate.Month)),
					_sum
				);
			}
		}
	}
}