﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace momo.acc.domain.Money
{
	public class EveryMonthThought : BaseMoneyThought, IMoneyThought
	{
		public EveryMonthThought(int sum)
		{
			_sum = sum;
		}

		public MoneyTalk[] GenerateFlow(DateTime day1, DateTime day2)
		{
			DateTime minDt = day1 > day2 ? day2 : day1;
			DateTime finDate = day2 > day1 ? day2 : day1;
			DateTime startDate = new DateTime(minDt.Year, minDt.Month, 1);

			return EnumerateTalks(startDate, finDate)
				.ToArray();
		}

		public MoneyTalk AggregateFlow(DateTime day1, DateTime day2)
		{
			DateTime firstDay = day1 > day2 ? day2 : day1;
			DateTime lastDay = day1 > day2 ? day1 : day2;
			firstDay = new DateTime(firstDay.Year, firstDay.Month, 1);
			lastDay = new DateTime(lastDay.Year, lastDay.Month, DateTime.DaysInMonth(lastDay.Year, lastDay.Month));
			int diff = (lastDay.Year - firstDay.Year) * 12 + lastDay.Month - firstDay.Month + 1;

			return new MoneyTalk(firstDay, lastDay, diff * _sum);
		}

		private readonly int _sum;

		private IEnumerable<MoneyTalk> EnumerateTalks(DateTime firstDayDate, DateTime finDate)
		{
			for (DateTime cDate = firstDayDate; cDate <= finDate; cDate = cDate.AddMonths(1))
			{
				yield return new MoneyTalk(
					cDate,
					new DateTime(cDate.Year, cDate.Month, DateTime.DaysInMonth(cDate.Year, cDate.Month)),
					_sum
				);
			}
		}
	}
}