﻿using System;

namespace momo.acc.domain.Money
{
	public abstract class BaseMoneyThought
	{
		public BaseMoneyThought(string id, string name)
		{
			_id = id;
			SetName(name);
		}

		public BaseMoneyThought()
			: this(Guid.NewGuid().ToString().Replace("-", ""), null)
		{ }

		public string Id { get { return _id; } }

		public string Name { get; private set; }

		public void SetName(string name)
		{
			Name = name;
		}

		private readonly string _id;
	}
}
