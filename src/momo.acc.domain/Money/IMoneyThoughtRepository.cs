﻿using System.Collections.Generic;
using bdb.infra.specs;

namespace momo.acc.domain.Money
{
	public interface IMoneyThoughtRepository
	{
		void Store(IMoneyThought moneyThought);

		void Remove(IMoneyThought moneyThought);

		IMoneyThought GetById(string id);

		IReadOnlyCollection<IMoneyThought> Find(ISpecification<IMoneyThought> specification);
	}
}
