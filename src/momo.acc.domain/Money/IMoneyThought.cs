﻿using System;

namespace momo.acc.domain.Money
{
	public interface IMoneyThought
	{
		string Id { get; }

		string Name { get; }

		MoneyTalk[] GenerateFlow(DateTime day1, DateTime day2);

		MoneyTalk AggregateFlow(DateTime day1, DateTime day2);
	}
}
