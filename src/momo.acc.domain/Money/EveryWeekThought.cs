﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace momo.acc.domain.Money
{
	public class EveryWeekThought : BaseMoneyThought, IMoneyThought
	{
		public EveryWeekThought(int sum)
		{
			_sum = sum;
		}

		public MoneyTalk[] GenerateFlow(DateTime day1, DateTime day2)
		{
			DateTime minDt = (day1 > day2 ? day2 : day1).Date;
			DateTime finDate = (day2 > day1 ? day2 : day1).Date;
			int diff = minDt.DayOfWeek - DayOfWeek.Monday;
			if (diff < 0)
			{
				diff += 7;
			}
			DateTime startDate = minDt.AddDays(-1 * diff);

			return EnumerateTalks(startDate, finDate)
				.ToArray();
		}

		public MoneyTalk AggregateFlow(DateTime day1, DateTime day2)
		{
			DateTime minDt = (day1 > day2 ? day2 : day1).Date;
			DateTime finDate = (day2 > day1 ? day2 : day1).Date;
			int diff = minDt.DayOfWeek - DayOfWeek.Monday;
			if (diff < 0)
				diff += 7;
			DateTime startDay = minDt.AddDays(-1 * diff);
			diff = finDate.DayOfWeek - DayOfWeek.Monday;
			if (diff < 0)
				diff += 7;
			DateTime finishDay = finDate.AddDays(-1 * diff + 6);
			int weeksCount = (int)((finishDay - startDay).Days / 7) + 1;
			return new MoneyTalk(startDay, finishDay, _sum * weeksCount);
		}

		private readonly int _sum;

		private IEnumerable<MoneyTalk> EnumerateTalks(DateTime firstDayDate, DateTime finDate)
		{
			for (DateTime cDate = firstDayDate; cDate <= finDate; cDate = cDate.AddDays(7))
			{
				yield return new MoneyTalk(
					cDate,
					cDate.AddDays(6),
					_sum
				);
			}
		}
	}
}