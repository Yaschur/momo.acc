﻿using System;

namespace momo.acc.domain.Money
{
	public class SingleDayThought : BaseMoneyThought, IMoneyThought
	{
		public SingleDayThought(int sum, DateTime dayDate)
		{
			_sum = sum;
			_dayDate = dayDate.Date;
		}

		public MoneyTalk AggregateFlow(DateTime day1, DateTime day2)
		{
			DateTime dd1 = day1.Date, dd2 = day2.Date;

			if (_dayDate > dd1 && _dayDate > dd2
				|| _dayDate < dd1 && _dayDate < dd2)
				return new MoneyTalk(default(DateTime), default(DateTime), 0);

			return new MoneyTalk(_dayDate, _dayDate, _sum);
		}

		public MoneyTalk[] GenerateFlow(DateTime day1, DateTime day2)
		{
			DateTime dd1 = day1.Date, dd2 = day2.Date;

			if (_dayDate > dd1 && _dayDate > dd2
				|| _dayDate < dd1 && _dayDate < dd2)
				return new MoneyTalk[0];

			return new[] { new MoneyTalk(_dayDate, _dayDate, _sum) };
		}

		private readonly DateTime _dayDate;
		private readonly int _sum;
	}
}