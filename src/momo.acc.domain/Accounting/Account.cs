﻿namespace momo.acc.domain.Accounting
{
	public class Account
	{
		public Account(string code, string name)
		{
			Code = code;
			ChangeName(name);
		}

		public string Code { get; private set; }

		public string Name { get; private set; }

		public void ChangeName(string name)
		{
			Name = name;
		}
	}
}
