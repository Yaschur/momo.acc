﻿using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using momo.acc.domain;
using momo.acc.domain.Timing;
using momo.acc.test._TestHelpers;
using NUnit.Framework;

namespace momo.acc.test.domain.Timing
{
	[TestFixture(Category = TestCategories.Timing)]
	public class DayShould
	{
		[Test]
		public void BeCreatedByConcreteDate()
		{
			Assert.DoesNotThrow(() => new Day(RndGenerator.GetRandomDateWithin(5000)));
		}

		[Test]
		public void ImplementIDays()
		{
			Day day = new Day(RndGenerator.GetRandomDateWithin(5000));

			Assert.IsInstanceOf<IDays>(day);
		}

		[Test]
		public void HaveSameFirstAndLastDays()
		{
			DateTime date = RndGenerator.GetRandomDateWithin(5000);

			Day day = new Day(date);

			Assert.AreEqual(day.LastDay, day.FirstDay);
		}

		[Test]
		public void EnumerateOneDate()
		{
			DateTime date = RndGenerator.GetRandomDateWithin(5000);
			Day day = new Day(date);

			List<DateTime> dates = day
				.Select(d => d)
				.ToList();

			Assert.AreEqual(1, dates.Count);
		}

		[Test]
		public void NormalizeTimeOfDatesToZero()
		{
			DateTime date = RndGenerator.GetRandomDateWithin(5000);

			Day day = new Day(date);

			Assert.AreEqual(TimeSpan.Zero, day.FirstDay.TimeOfDay);
			Assert.AreEqual(TimeSpan.Zero, day.LastDay.TimeOfDay);
			Assert.IsTrue(day.All(dt => dt.TimeOfDay == TimeSpan.Zero));
		}

		[TestCase(-10, -1, Result = true)]
		[TestCase(-1, 0, Result = true)]
		[TestCase(-1, 1, Result = true)]
		[TestCase(0, 0, Result = false)]
		[TestCase(0, 1, Result = false)]
		[TestCase(1, 1, Result = false)]
		[TestCase(1, 10, Result = false)]
		public bool DetectPrecedingPeriods(int before, int after)
		{
			IDays anotherPeriod = A.Fake<IDays>();
			DateTime trgDate = DateTime.Now.Date;
			DateTime
				start = trgDate.AddDays(before),
				end = trgDate.AddDays(after);
			A.CallTo(() => anotherPeriod.FirstDay)
				.Returns(start);
			A.CallTo(() => anotherPeriod.LastDay)
				.Returns(end);
			Day day = new Day(trgDate);

			return day.IsFinishedAfter(anotherPeriod);
		}

		[TestCase(-1, 1, Result = true)]
		[TestCase(0, 10, Result = true)]
		[TestCase(1, 10, Result = true)]
		[TestCase(0, 0, Result = false)]
		[TestCase(-1, 0, Result = false)]
		[TestCase(-1, -1, Result = false)]
		[TestCase(-1, -10, Result = false)]
		public bool DetectFollowingPeriods(int before, int after)
		{
			IDays anotherPeriod = A.Fake<IDays>();
			DateTime trgDate = DateTime.Now.Date;
			DateTime
				start = trgDate.AddDays(before),
				end = trgDate.AddDays(after);
			A.CallTo(() => anotherPeriod.FirstDay)
				.Returns(start);
			A.CallTo(() => anotherPeriod.LastDay)
				.Returns(end);
			Day day = new Day(trgDate);

			return day.IsStartedBefore(anotherPeriod);
		}

		[TestCase(-1, 0, Result = true)]
		[TestCase(-5, 3, Result = true)]
		[TestCase(0, 10, Result = true)]
		[TestCase(0, 0, Result = true)]
		[TestCase(-1, -1, Result = false)]
		[TestCase(-1, -10, Result = false)]
		[TestCase(3, 3, Result = false)]
		[TestCase(1, 5, Result = false)]
		public bool DetectIntersetedPeriods(int before, int after)
		{
			IDays anotherPeriod = A.Fake<IDays>();
			DateTime trgDate = DateTime.Now.Date;
			DateTime
				start = trgDate.AddDays(before),
				end = trgDate.AddDays(after);
			A.CallTo(() => anotherPeriod.FirstDay)
				.Returns(start);
			A.CallTo(() => anotherPeriod.LastDay)
				.Returns(end);
			Day day = new Day(trgDate);

			return day.IsIntersectedBy(anotherPeriod);
		}
	}
}
