﻿using System;
using System.Linq;
using momo.acc.domain;
using momo.acc.domain.Timing;
using momo.acc.test._TestHelpers;
using NUnit.Framework;

namespace momo.acc.test.domain.Timing
{
	[TestFixture(Category = TestCategories.Timing)]
	public class SolidDaysShould
	{
		[Test]
		public void ShouldBeCreatedByTwoDates()
		{
			Assert.DoesNotThrow(() => new SolidDays(
				RndGenerator.GetRandomDateWithin(5000),
				RndGenerator.GetRandomDateWithin(5000)
			));
		}

		[Test]
		public void ImplementIDays()
		{
			SolidDays solidDays = new SolidDays(RndGenerator.GetRandomDateWithin(5000), RndGenerator.GetRandomDateWithin(5000));

			Assert.IsInstanceOf<IDays>(solidDays);
		}

		[Test]
		public void SetFirstLastDays()
		{
			DateTime
				date1 = RndGenerator.GetRandomDateWithin(5000).Date,
				date2 = RndGenerator.GetRandomDateWithin(5000).Date;
			DateTime
				expFirst = date1 > date2 ? date2 : date1,
				expLast = date1 > date2 ? date1 : date2;

			SolidDays solidDays1 = new SolidDays(date1, date2);
			SolidDays solidDays2 = new SolidDays(date2, date1);

			Assert.AreEqual(expFirst, solidDays1.FirstDay);
			Assert.AreEqual(expLast, solidDays1.LastDay);
			Assert.AreEqual(expFirst, solidDays2.FirstDay);
			Assert.AreEqual(expLast, solidDays2.LastDay);
		}

		[Test]
		public void EnumerateAllDateBetweenFirstAndLast()
		{
			DateTime
				date1 = RndGenerator.GetDateBetween(-100, -50).Date,
				date2 = RndGenerator.GetDateBetween(-30, 10).Date;
			SolidDays solidDays = new SolidDays(date1, date2);
			DateTime[] expDates = Enumerable
				.Range(0, (date2 - date1).Days + 1)
				.Select(i => date1.AddDays(i))
				.ToArray();

			DateTime[] actDates = solidDays.ToArray();

			Assert.AreEqual(expDates, actDates);
		}

		[Test]
		public void NormalizeTimeOfDatesToZero()
		{
			DateTime
				date1 = RndGenerator.GetRandomDateWithin(5000),
				date2 = RndGenerator.GetRandomDateWithin(5000);

			SolidDays solidDays = new SolidDays(date1, date2);

			Assert.AreEqual(TimeSpan.Zero, solidDays.FirstDay.TimeOfDay);
			Assert.AreEqual(TimeSpan.Zero, solidDays.LastDay.TimeOfDay);
			Assert.IsTrue(solidDays.All(dt => dt.TimeOfDay == TimeSpan.Zero));
		}
	}
}
