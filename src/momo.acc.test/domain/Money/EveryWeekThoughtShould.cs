﻿using System;
using momo.acc.domain.Money;
using momo.acc.test._TestHelpers;
using NUnit.Framework;

namespace momo.acc.test.domain.Money
{
	[TestFixture(Category = TestCategories.MoneyTalk)]
	public class EveryWeekThoughtShould
	{
		[Test]
		public void BeCreatedByAmount()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);

			Assert.DoesNotThrow(() => new EveryWeekThought(sum));
		}

		[Test]
		public void ImplementIMoneyThought()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);

			EveryWeekThought thougth = new EveryWeekThought(sum);

			Assert.IsInstanceOf<IMoneyThought>(thougth);
		}

		[Test]
		public void GenerateWeekAlignedTalks()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);
			DateTime trgDate = RndGenerator.GetRandomDateWithin(100);
			int lOffset = RndGenerator.GetNumberBetween(-1000, 0), rOffset = RndGenerator.GetNumberBetween(0, 1000);
			DateTime
				p1 = trgDate.AddDays(7 * lOffset),
				p2 = trgDate.AddDays(7 * rOffset);
			int expCount = rOffset - lOffset + 1;
			EveryWeekThought thougth = new EveryWeekThought(sum);

			MoneyTalk[] talks = thougth.GenerateFlow(p1, p2);

			Assert.AreEqual(expCount, talks.Length);
			for (int i = 0; i < talks.Length; i++)
			{
				Assert.AreEqual(sum, talks[i].Sum);
				Assert.AreEqual(DayOfWeek.Monday, talks[i].FirstDay.DayOfWeek);
				Assert.AreEqual(DayOfWeek.Sunday, talks[i].LastDay.DayOfWeek);
				if (i == talks.Length - 1)
				{
					int diff = (talks[i].LastDay - p2).Days;
					Assert.LessOrEqual(diff, 7);
				}
				if (i == 0)
				{
					int diff = (p1 - talks[i].FirstDay).Days;
					Assert.LessOrEqual(diff, 7);
					continue;
				}
				Assert.AreEqual(1, (talks[i].FirstDay - talks[i - 1].LastDay).Days);
			}
		}

		[Test]
		public void AggregateSumByNumberOfWeeksInPeriod()
		{
			int weeks = RndGenerator.GetNumberBetween(-100, 100);
			DateTime
				p1 = RndGenerator.GetRandomDateWithin(1000),
				p2 = p1.AddDays(weeks * 7);
			int sum = RndGenerator.GetNumberBetween(-333333, 33333);
			EveryWeekThought thougth = new EveryWeekThought(sum);
			int expSum = (Math.Abs(weeks) + 1) * sum;

			MoneyTalk aggTalk = thougth.AggregateFlow(p1, p2);

			Assert.AreEqual(expSum, aggTalk.Sum);
		}

		[Test]
		public void AlignTalkToExtremeDayOfWeeksInPeriodOnAggregate()
		{
			DateTime
				p1 = RndGenerator.GetDateBetween(-1000, 0),
				p2 = RndGenerator.GetDateBetween(0, 1000);
			int sum = RndGenerator.GetNumberBetween(-333333, 33333);
			EveryWeekThought thougth = new EveryWeekThought(sum);
			int diff = p1.DayOfWeek - DayOfWeek.Monday;
			if (diff < 0)
				diff += 7;
			DateTime expFirstDay = p1.AddDays(-1 * diff);
			diff = p2.DayOfWeek - DayOfWeek.Monday;
			if (diff < 0)
				diff += 7;
			DateTime expLastDay = p2.AddDays(-1 * diff + 6);

			MoneyTalk aggTalk = thougth.AggregateFlow(p1, p2);

			Assert.AreEqual(expFirstDay.Date, aggTalk.FirstDay);
			Assert.AreEqual(DayOfWeek.Monday, aggTalk.FirstDay.DayOfWeek);
			Assert.AreEqual(expLastDay.Date, aggTalk.LastDay);
			Assert.AreEqual(DayOfWeek.Sunday, aggTalk.LastDay.DayOfWeek);
		}
	}
}
