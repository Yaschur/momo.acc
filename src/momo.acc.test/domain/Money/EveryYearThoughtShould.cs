﻿using System;
using momo.acc.domain.Money;
using momo.acc.test._TestHelpers;
using NUnit.Framework;

namespace momo.acc.test.domain.Money
{
	[TestFixture(Category = TestCategories.MoneyTalk)]
	public class EveryYearThoughtShould
	{
		[Test]
		public void BeCreatedByAmount()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);

			Assert.DoesNotThrow(() => new EveryYearThought(sum));
		}

		[Test]
		public void ImplementIMoneyThought()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);

			EveryYearThought thougth = new EveryYearThought(sum);

			Assert.IsInstanceOf<IMoneyThought>(thougth);
		}

		[Test]
		public void GenerateYearAlignedTalks()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);
			DateTime trgDate = RndGenerator.GetRandomDateWithin(100);
			int lOffset = RndGenerator.GetNumberBetween(-20, 0), rOffset = RndGenerator.GetNumberBetween(0, 20);
			DateTime
				p1 = trgDate.AddYears(lOffset),
				p2 = trgDate.AddYears(rOffset);
			int expCount = rOffset - lOffset + 1;
			EveryYearThought thougth = new EveryYearThought(sum);

			MoneyTalk[] talks = thougth.GenerateFlow(p1, p2);

			Assert.AreEqual(expCount, talks.Length);
			for (int i = 0; i < talks.Length; i++)
			{
				MoneyTalk talk = talks[i];
				DateTime expFirst = new DateTime(talk.FirstDay.Year, 1, 1);
				DateTime expLast = new DateTime(talk.LastDay.Year, 12, DateTime.DaysInMonth(talk.LastDay.Year, 12));

				Assert.AreEqual(sum, talk.Sum);
				Assert.AreEqual(expFirst, talk.FirstDay);
				Assert.AreEqual(expLast, talk.LastDay);
				Assert.AreEqual(talk.FirstDay.Year, talk.LastDay.Year);

				if (i == talks.Length - 1)
					Assert.AreEqual(p2.Year, talk.LastDay.Year);
				if (i == 0)
				{
					Assert.AreEqual(p1.Year, talk.FirstDay.Year);
					continue;
				}
				Assert.AreEqual(1, talk.FirstDay.Year - talks[i - 1].LastDay.Year);
			}
		}

		[Test]
		public void AggregateSumByNumberOfYearsInPeriod()
		{
			DateTime
				p1 = RndGenerator.GetRandomDateWithin(1000),
				p2 = RndGenerator.GetRandomDateWithin(1000);
			int sum = RndGenerator.GetNumberBetween(-333333, 33333);
			EveryYearThought thougth = new EveryYearThought(sum);
			int yearCount = Math.Abs(p1.Year - p2.Year) + 1;
			int expSum = sum * yearCount;

			MoneyTalk aggTalk = thougth.AggregateFlow(p1, p2);

			Assert.AreEqual(expSum, aggTalk.Sum);
		}

		[Test]
		public void AlignTalkToExtremeDayOfYearsInPeriodOnAggregate()
		{
			DateTime
				p1 = RndGenerator.GetDateBetween(-5000, 0),
				p2 = RndGenerator.GetDateBetween(0, 5000);
			int sum = RndGenerator.GetNumberBetween(-333333, 33333);
			EveryYearThought thougth = new EveryYearThought(sum);
			DateTime expFirstDay = new DateTime(p1.Year, 1, 1);
			DateTime expLastDay = new DateTime(p2.Year, 12, DateTime.DaysInMonth(p2.Year, 12));

			MoneyTalk aggTalk = thougth.AggregateFlow(p1, p2);

			Assert.AreEqual(expFirstDay, aggTalk.FirstDay);
			Assert.AreEqual(expLastDay, aggTalk.LastDay);
		}
	}
}
