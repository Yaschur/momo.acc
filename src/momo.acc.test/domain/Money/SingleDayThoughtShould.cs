﻿using System;
using momo.acc.domain.Money;
using momo.acc.test._TestHelpers;
using NUnit.Framework;

namespace momo.acc.test.domain.Money
{
	[TestFixture(Category = TestCategories.MoneyTalk)]
	public class SingleDayThoughtShould
	{
		[Test]
		public void BeCreatedByAmountAndDate()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);
			DateTime dt = RndGenerator.GetRandomDateWithin(100);

			Assert.DoesNotThrow(() => new SingleDayThought(sum, dt));
		}

		[Test]
		public void ImplementIMoneyThought()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);
			DateTime dt = RndGenerator.GetRandomDateWithin(100);

			SingleDayThought dayThought = new SingleDayThought(sum, dt);

			Assert.IsInstanceOf<IMoneyThought>(dayThought);
		}

		[Test]
		public void GenerateOneTalk()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);
			DateTime dt = RndGenerator.GetRandomDateWithin(100);
			DateTime
				p1 = RndGenerator.GetDateBetween(-1000, -100),
				p2 = RndGenerator.GetDateBetween(100, 1000);
			SingleDayThought dayThought = new SingleDayThought(sum, dt);
			DateTime expDt = dt.Date;

			MoneyTalk[] res = dayThought.GenerateFlow(p1, p2);

			Assert.AreEqual(1, res.Length);
			Assert.AreEqual(expDt, res[0].FirstDay);
			Assert.AreEqual(expDt, res[0].LastDay);
			Assert.AreEqual(sum, res[0].Sum);
		}

		[Test]
		public void AggregateSumInSingleDay()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);
			DateTime dt = RndGenerator.GetRandomDateWithin(100);
			DateTime
				p1 = RndGenerator.GetDateBetween(-1000, -100),
				p2 = RndGenerator.GetDateBetween(100, 1000);
			SingleDayThought dayThought = new SingleDayThought(sum, dt);
			DateTime expDt = dt.Date;

			MoneyTalk res = dayThought.AggregateFlow(p1, p2);

			Assert.AreEqual(expDt, res.FirstDay);
			Assert.AreEqual(expDt, res.LastDay);
			Assert.AreEqual(sum, res.Sum);
		}

		[Test]
		public void GenerateOrAggregateOnDayPeriod()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);
			DateTime dt = RndGenerator.GetRandomDateWithin(100);
			SingleDayThought dayThought = new SingleDayThought(sum, dt);
			DateTime expDt = dt.Date;

			MoneyTalk[] resG = dayThought.GenerateFlow(dt, dt);
			MoneyTalk resA = dayThought.AggregateFlow(dt, dt);


			Assert.AreEqual(1, resG.Length);
			Assert.AreEqual(expDt, resG[0].FirstDay);
			Assert.AreEqual(expDt, resG[0].LastDay);
			Assert.AreEqual(sum, resG[0].Sum);

			Assert.AreEqual(expDt, resA.FirstDay);
			Assert.AreEqual(expDt, resA.LastDay);
			Assert.AreEqual(sum, resA.Sum);
		}

		[Test]
		public void GenerateOrAggregateEmptiesIfOutOfDayPeriod()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);
			DateTime dt = RndGenerator.GetRandomDateWithin(100);
			SingleDayThought dayThought = new SingleDayThought(sum, dt);
			DateTime p1 = dt.AddDays(5), p2 = dt.AddDays(10);

			MoneyTalk[] resG = dayThought.GenerateFlow(p1, p2);
			MoneyTalk resA = dayThought.AggregateFlow(p1, p2);


			Assert.AreEqual(0, resG.Length);

			Assert.AreEqual(default(DateTime), resA.FirstDay);
			Assert.AreEqual(default(DateTime), resA.LastDay);
			Assert.AreEqual(0, resA.Sum);
		}
	}
}
