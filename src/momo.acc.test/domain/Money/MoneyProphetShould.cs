﻿using System;
using FakeItEasy;
using momo.acc.domain.Money;
using momo.acc.test._TestHelpers;
using NUnit.Framework;

namespace momo.acc.test.domain.Money
{
	[TestFixture(Category = TestCategories.MoneyTalk)]
	public class MoneyProphetShould
	{
		MoneyProphet moneyProphetUt;

		[SetUp]
		public void Setup()
		{
			moneyProphetUt = new MoneyProphet();
		}

		[Test]
		public void AggregateAddedThoughtsOnCalculatingFutureBalance()
		{
			int initialBalance = 100;
			DateTime
				initialDay = DateTime.Now,
				targetDay = DateTime.Now.AddMonths(3);
			IMoneyThought
				thought1 = A.Fake<IMoneyThought>(),
				thought2 = A.Fake<IMoneyThought>(),
				thought3 = A.Fake<IMoneyThought>();
			moneyProphetUt.Add(thought1);
			moneyProphetUt.Add(thought2);
			moneyProphetUt.Add(thought3);
			moneyProphetUt.StrikeBalance(initialBalance, targetDay);

			moneyProphetUt.CalculateBalanceOn(targetDay);

			A.CallTo(() => thought1.AggregateFlow(initialDay, targetDay))
				.MustHaveHappened(Repeated.Exactly.Once);
			A.CallTo(() => thought2.AggregateFlow(initialDay, targetDay))
				.MustHaveHappened(Repeated.Exactly.Once);
			A.CallTo(() => thought3.AggregateFlow(initialDay, targetDay))
				.MustHaveHappened(Repeated.Exactly.Once);
		}
	}
}
