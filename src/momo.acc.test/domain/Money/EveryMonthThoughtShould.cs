﻿using System;
using momo.acc.domain.Money;
using momo.acc.test._TestHelpers;
using NUnit.Framework;

namespace momo.acc.test.domain.Money
{
	[TestFixture(Category = TestCategories.MoneyTalk)]
	public class EveryMonthThoughtShould
	{
		[Test]
		public void BeCreatedByAmount()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);

			Assert.DoesNotThrow(() => new EveryMonthThought(sum));
		}

		[Test]
		public void ImplementIMoneyThought()
		{
			int sum = RndGenerator.GetNumberBetween(-33333, 33333);

			EveryMonthThought thougth = new EveryMonthThought(sum);

			Assert.IsInstanceOf<IMoneyThought>(thougth);
		}

		[TestCase(0, 0, 1)]
		[TestCase(-1, 0, 2)]
		[TestCase(-1, 1, 3)]
		[TestCase(-5, 3, 9)]
		public void GenerateMonthAlignedTalks(int leftMonthOffset, int rightMonthOffset, int talksCount)
		{
			DateTime trgDate = RndGenerator.GetRandomDateWithin(10);
			DateTime p1 = trgDate.AddMonths(leftMonthOffset), p2 = trgDate.AddMonths(rightMonthOffset);
			int sum = RndGenerator.GetNumberBetween(-333333, 33333);
			EveryMonthThought thougth = new EveryMonthThought(sum);

			MoneyTalk[] talks = thougth.GenerateFlow(p1, p2);

			Assert.AreEqual(talksCount, talks.Length);
			foreach (var talk in talks)
			{
				Assert.AreEqual(sum, talk.Sum);
				Assert.AreEqual(1, talk.FirstDay.Day);
				Assert.AreEqual(talk.FirstDay.Month, talk.LastDay.Month);
				Assert.AreEqual(talk.FirstDay.Year, talk.LastDay.Year);
				DateTime nextMonth = talk.LastDay.AddDays(1);
				Assert.AreNotEqual(talk.LastDay.Month, nextMonth.Month);
			}
		}

		[Test]
		public void AggregateSumByNumberOfMonthsInPeriod()
		{
			DateTime
				p1 = RndGenerator.GetRandomDateWithin(1000),
				p2 = RndGenerator.GetRandomDateWithin(1000);
			int sum = RndGenerator.GetNumberBetween(-333333, 33333);
			EveryMonthThought thougth = new EveryMonthThought(sum);
			int monthCount = Math.Abs(
				(p1.Year - p2.Year) * 12 + p1.Month - p2.Month
			) + 1;
			int expSum = sum * monthCount;

			MoneyTalk aggTalk = thougth.AggregateFlow(p1, p2);

			Assert.AreEqual(expSum, aggTalk.Sum);
		}

		[Test]
		public void AlignTalkToExtremeDayOfMonthsInPeriodOnAggregate()
		{
			DateTime
				p1 = RndGenerator.GetDateBetween(-1000, 0),
				p2 = RndGenerator.GetDateBetween(0, 1000);
			int sum = RndGenerator.GetNumberBetween(-333333, 33333);
			EveryMonthThought thougth = new EveryMonthThought(sum);
			DateTime expFirstDay = new DateTime(p1.Year, p1.Month, 1);
			DateTime expLastDay = new DateTime(p2.Year, p2.Month, DateTime.DaysInMonth(p2.Year, p2.Month));

			MoneyTalk aggTalk = thougth.AggregateFlow(p1, p2);

			Assert.AreEqual(expFirstDay, aggTalk.FirstDay);
			Assert.AreEqual(expLastDay, aggTalk.LastDay);
		}
	}
}
