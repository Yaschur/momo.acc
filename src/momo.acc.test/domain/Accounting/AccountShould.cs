﻿using momo.acc.domain.Accounting;
using NUnit.Framework;

namespace momo.acc.test.domain.Accounting
{
	[TestFixture]
	public class AccountShould
	{
		[Test]
		public void BeCreatedByCodeAndName()
		{
			string code = "123", name = "456";

			Account account = new Account(code, name);

			Assert.AreEqual(code, account.Code);
			Assert.AreEqual(name, account.Name);
		}
	}
}
