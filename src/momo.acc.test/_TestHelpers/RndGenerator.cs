﻿using System;
using System.Linq;

namespace momo.acc.test._TestHelpers
{
	public static class RndGenerator
	{
		internal static int GetNumberBetween(int minIncluded, int maxIncluded)
		{
			return random.Next(minIncluded, maxIncluded + 1);
		}

		internal static string GetAlphaNumString(int size)
		{
			char[] str = Enumerable.Range(0, Math.Abs(size))
				.Select(x => getRandomChar())
				.ToArray();
			return new string(str);
		}

		internal static bool GetBoolean()
		{
			return 1 == random.Next(0, 2);
		}

		internal static DateTime GetDateBetween(DateTime beginIncluded, DateTime endIncluded)
		{
			int days = (int)(endIncluded - beginIncluded).TotalDays;

			return beginIncluded.AddDays(random.Next(0, days));
		}

		internal static DateTime GetDateBetween(int daysBeforeNow, int daysAfterNow)
		{
			DateTime begDate = DateTime.Now.AddDays(daysBeforeNow);
			DateTime endDate = DateTime.Now.AddDays(daysAfterNow);

			return GetDateBetween(begDate, endDate);
		}

		internal static DateTime GetRandomDateWithin(int daysAroundNow)
		{
			return GetDateBetween(-daysAroundNow, daysAroundNow);
		}

		private static Random random = new Random();
		private const string alphaNums = "abcdefghijklmnopqrstuvwxyz0123456789";

		private static char getRandomChar()
		{
			char alphaNum = alphaNums[random.Next(0, alphaNums.Length)];
			return GetBoolean() ? char.ToUpper(alphaNum) : alphaNum;
		}

	}
}
